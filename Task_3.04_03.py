"""
Имеется файл с данными по успеваемости абитуриентов. Он представляет из себя набор строк,
где в каждой строке записана следующая информация:
Фамилия;Оценка_по_математике;Оценка_по_физике;Оценка_по_русскому_языку
Поля внутри строки разделены точкой с запятой, оценки — целые числа.
Напишите программу, которая считывает исходный файл с подобной структурой и для каждого
абитуриента записывает его среднюю оценку по трём предметам на отдельной строке,
соответствующей этому абитуриенту, в файл с ответом.
Также вычислите средние баллы по математике, физике и русскому языку по всем абитуриентам
и добавьте полученные значения, разделённые пробелом, последней строкой в файл с ответом.
В качестве ответа на задание прикрепите полученный файл со средними оценками по каждому
ученику и одной строкой со средними оценками по трём предметам.
"""
import os
# файл с входными данными
i_file_name: str = os.path.join("/", "Users", "bearcat", "Downloads", "dataset_3363_4.txt")
# файл с выходными данными
o_file_name: str = os.path.join("/", "Users", "bearcat", "Downloads", "output.txt")
# данные из считаной строки файла
data_list = []
# результаты
math_score: float = 0.0
physics_score: float = 0.0
russian_score: float = 0.0
student_results = []
# чтение данных из входящего файла
student_count: int = 0
with open(i_file_name, "r") as input_file:
    for text_line in input_file:
        # читаем данные из строки файла в список
        data_list = text_line.strip().split(";")
        student_count += 1
        # средняя очценка студента
        student_avrg: float = 0.0
        for i in range(1, len(data_list)):
            student_avrg += float(data_list[i])
        student_avrg = student_avrg / (len(data_list) - 1)
        student_results.append(student_avrg)
        # средняя оценка по предметам: математика;физика;русский
        math_score += float(data_list[1])
        physics_score += float(data_list[2])
        russian_score += float(data_list[3])
math_score = math_score / student_count
physics_score = physics_score / student_count
russian_score = russian_score / student_count
# запись результатов в выходной файл
with open(o_file_name, "w") as output_file:
    for score in student_results:
        output_file.write(str(score) + "\n")
    output_file.write("{0} {1} {2}".format(str(math_score), str(physics_score), str(russian_score)))
