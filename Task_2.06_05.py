"""
Выведите таблицу размером n×n, заполненную числами от 11 до n^2
по спирали, выходящей из левого верхнего угла и закрученной по часовой
стрелке, как показано в примере (здесь n=5):
1  2  3  4  5
16 17 18 19 6
15 24 25 20 7
14 23 22 21 8
13 12 11 10 9
"""
# размер матрицы
matrix_size: int = int(input())
# создание и иницилизация матрицы нулями
matrix = [[0] * matrix_size for k in range(matrix_size)]
# индекс строки
i: int = 0
# индекс колонки
j: int = 0
# заполняем матрицу числами от 1 до matrix_size в квадрате
for number in range(1, matrix_size ** 2 + 1):
    matrix[i][j] = number
    if i <= j + 1 and i + j < matrix_size - 1:
        j += 1
    elif i < j:
        i += 1
    elif i + j >= matrix_size:
        j -= 1
    elif i >= j:
        i -= 1
# вывод заполненой матрицы
for index in range(matrix_size):
    print(*matrix[index])
