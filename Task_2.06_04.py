"""
Напишите программу, на вход которой подаётся прямоугольная матрица в виде
последовательности строк. После последней строки матрицы идёт строка,
содержащая только строку "end" (без кавычек, см. Sample Input).
Программа должна вывести матрицу того же размера, у которой каждый элемент
в позиции i, j равен сумме элементов первой матрицы на позициях (i-1, j),
(i+1, j), (i, j-1), (i, j+1). У крайних символов соседний элемент находится
с противоположной стороны матрицы.
В случае одной строки/столбца элемент сам себе является соседом по
соответствующему направлению.
"""
# матрица чисел
matrix = []
while True:
    input_line: str = str(input())
    if input_line == "end":
        break
    matrix_line = [int(i) for i in input_line.split()]
    matrix.append(matrix_line)
# расчет новой матрицы
for i in range(len(matrix)):
    # строка новой матрицы
    Result: str = ""
    for j in range(len(matrix_line)):
        a = int(matrix[i - 1][j])
        if i == len(matrix) - 1:
            b = int(matrix[0][j])
        else:
            b = int(matrix[i + 1][j])
        c = int(matrix[i][j - 1])
        if j == len(matrix[i]) - 1:
            d = int(matrix[i][0])
        else:
            d = int(matrix[i][j + 1])
        new_number = a + b + c + d
        Result += str(new_number) + " "
    # вывод строки новой матрицы
    print(Result.rstrip())
