"""
Недавно мы считали для каждого слова количество его вхождений в строку. Но на все
слова может быть не так интересно смотреть, как, например, на наиболее часто используемые.
Напишите программу, которая считывает текст из файла (в файле может быть больше одной
строки) и выводит самое частое слово в этом тексте и через пробел то, сколько раз оно
встретилось. Если таких слов несколько, вывести лексикографически первое (можно
использовать оператор < для строк).
В качестве ответа укажите вывод программы, а не саму программу.
Слова, написанные в разных регистрах, считаются одинаковыми.
"""
import os
# файл с входными данными
i_file_name: str = os.path.join("/", "Users", "bearcat", "Downloads", "dataset_3363_3.txt")
# файл с выходными данными
o_file_name: str = os.path.join("/", "Users", "bearcat", "Downloads", "output.txt")
# текст из входного файла для обработки
text_for_process: str = ""
# результат обработки
result_word: str = ""
# чтение текста из входящего файла
with open(i_file_name, "r") as input_file:
    for text_line in input_file:
        if text_for_process == "":
            text_for_process = text_line.lower().strip()
        else:
            text_for_process += " " + text_line.lower().strip()
# определение самого часто используемого слова
# максимально число раз, которое встречается слово в тексте
max_count: int = 0
word_list = text_for_process.split()
word_list.sort()
for word in word_list:
    counter = word_list.count(word)
    if counter > max_count:
        max_count = counter
        result_word = word
# запись обработанного текста в выходной файл
with open(o_file_name, "w") as output_file:
    output_file.write(result_word + " " + str(max_count))
