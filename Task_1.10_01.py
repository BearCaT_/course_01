"""
Из передачи “Здоровье” Аня узнала, что рекомендуется спать хотя бы AA часов в сутки,
но пересыпать тоже вредно и не стоит спать более BB часов. Сейчас Аня спит HH часов
в сутки. Если режим сна Ани удовлетворяет рекомендациям передачи “Здоровье”, выведите
“Это нормально”. Если Аня спит менее AA часов, выведите “Недосып”, если же более BB
часов, то выведите “Пересып”.
Получаемое число AA всегда меньше либо равно BB.
На вход программе в три строки подаются переменные в следующем порядке: AA, BB, HH.
"""
min_sleep_time_h = int(input())
max_sleep_time_h = int(input())
current_sleep_time_h = int(input())
if min_sleep_time_h <= current_sleep_time_h <= max_sleep_time_h:
    print("Это нормально")
elif current_sleep_time_h < min_sleep_time_h:
    print("Недосып")
elif current_sleep_time_h > max_sleep_time_h:
    print("Пересып")
