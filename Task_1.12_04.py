"""
Жители страны Малевии часто экспериментируют с планировкой комнат. Комнаты
бывают треугольные, прямоугольные и круглые. Чтобы быстро вычислять жилплощадь,
требуется написать программу, на вход которой подаётся тип фигуры комнаты и
соответствующие параметры, которая бы выводила площадь получившейся комнаты.
Для числа π в стране Малевии используют значение 3.14.
Формат ввода, который используют Малевийцы:
треугольник a b c
где a, b и c — длины сторон треугольника
прямоугольник a b
где a и b — длины сторон прямоугольника
круг r
где r — радиус окружности
"""
TRIANGLE: str = "треугольник"
RECTANGLE: str = "прямоугольник"
CIRCLE: str = "круг"
PI: float = 3.14
shape_type: str = input()
if shape_type == TRIANGLE:
    side_a = int(input())
    side_b = int(input())
    side_c = int(input())
    p_2 = (side_a + side_b + side_c) / 2
    S: float = (p_2 * (p_2 - side_a) * (p_2 - side_b) * (p_2 - side_c)) ** 0.5
    print(S)
elif shape_type == RECTANGLE:
    side_a = int(input())
    side_b = int(input())
    S: float = side_a * side_b
    print(S)
elif shape_type == CIRCLE:
    radius = int(input())
    S: float = PI * (radius ** 2)
    print(S)
