# ======================================================================================
# MAIN.PY
# ======================================================================================
"""
Имеется набор файлов, каждый из которых, кроме последнего, содержит имя следующего файла.
Первое слово в тексте последнего файла: "We".
Скачайте предложенный файл. В нём содержится ссылка на первый файл из этого набора.
Все файлы располагаются в каталоге по адресу:
https://stepic.org/media/attachments/course67/3.6.3/
Загрузите содержимое последнего файла из набора, как ответ на это задание.
"""
import requests
import lib_01
# получаем url первого файла в онлайн каталоге
url: str = lib_01.get_file_url()
# получаем url онлайн каталога
catalog_url: str = lib_01.get_catalog_url(url)
# бесконечный цикл
while True:
    # чтение первой строки текста по адресу URL
    first_file_line: str = requests.get(url).text.splitlines()[0]
    # чтение первого слова из первой строки
    first_word: str = first_file_line.split()[0]
    # если первое слово в первой строке равно кодовому - печать текста файла и выход
    if first_word == "We":
        print(requests.get(url).text)
        break
    # если первая строка текста файла = имя следующего файла в каталоге
    url = catalog_url + first_file_line.strip()


# ======================================================================================
# LIB_O1.PY
# ======================================================================================
import os
def get_catalog_url(url: str):
    # индекс цикла
    ind: int = 0
    # url каталога
    catalog_url: str = ""
    for url_part in url.split("/"):
        ind += 1
        catalog_url += url_part + "/"
        if ind == len(url.split("/")) - 1:
            break
    return catalog_url


def get_file_url():
    # файл с входными данными
    i_file_name: str = os.path.join("/", "Users", "bearcat", "Downloads", "dataset_3378_3.txt")
    # чтение http адреса из первой строки входящего файла
    with open(i_file_name, "r") as input_file:
        return input_file.readline().strip()
