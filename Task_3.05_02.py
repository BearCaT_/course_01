"""
Напишите программу, которая запускается из консоли и печатает значения всех переданных
аргументов на экран (имя скрипта выводить не нужно). Не изменяйте порядок аргументов
при выводе.
Для доступа к аргументам командной строки программы подключите модуль sys и используйте
переменную argv из этого модуля.
"""
import sys
first_time = True
Result: str = ""
D = sys.argv
# цикл
for argument in sys.argv:
    if first_time:
        first_time = False
        continue
    Result += argument + " "
print(Result.strip())
