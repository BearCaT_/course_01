"""
Напишите программу, на вход которой подается одна строка с целыми числами.
Программа должна вывести сумму этих чисел.
Используйте метод split строки.
"""
number_list = [int(i) for i in input().split()]
total_sum: int = 0
for i in number_list:
    total_sum += i
print(total_sum)
