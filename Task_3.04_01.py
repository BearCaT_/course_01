"""
На прошлой неделе мы сжимали строки, используя кодирование повторов. Теперь
нашей задачей будет восстановление исходной строки обратно. Напишите программу,
которая считывает из файла строку, соответствующую тексту, сжатому с помощью
кодирования повторов, и производит обратную операцию, получая исходный текст.
Запишите полученный текст в файл и прикрепите его, как ответ на это задание.
В исходном тексте не встречаются цифры, так что код однозначно интерпретируем.
Примечание. Это первое задание типа Dataset Quiz. В таких заданиях после нажатия
"Start Quiz" у вас появляется ссылка "download your dataset". Используйте эту
ссылку для того, чтобы загрузить файл со входными данными к себе на компьютер.
Запустите вашу программу, используя этот файл в качестве входных данных.
Выходной файл, который при этом у вас получится, надо отправить в качестве
ответа на эту задачу.
"""
import os
# файл с входными данными
i_file_name: str = os.path.join("/", "Users", "bearcat", "Downloads", "dataset_3363_2.txt")
# файл с выходными данными
o_file_name: str = os.path.join("/", "Users", "bearcat", "Downloads", "output.txt")
# текст из входного файла для обработки
text_for_process: str = ""
# результат обработки
Result: str = ""
# чтение текста из входящего файла
with open(i_file_name, "r") as input_file:
    for text_line in input_file:
        text_for_process += text_line.strip()
# распаковка считаного текста
char_index: int = 0
number_index: int = 0
for char in text_for_process:
    char_index += 1
    if char.isalpha():
        number_index = char_index
        number_of_char_s: str = ""
        while number_index < len(text_for_process) and text_for_process[number_index].isdigit():
            number_of_char_s += text_for_process[number_index]
            number_index += 1
        Result += char*int(number_of_char_s)
# запись обработанного текста в выходной файл
with open(o_file_name, "w") as output_file:
    output_file.write(Result)
