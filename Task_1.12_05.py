"""
Напишите программу, которая получает на вход три целых числа, по одному
числу в строке, и выводит на консоль в три строки сначала максимальное,
потом минимальное, после чего оставшееся число.
На ввод могут подаваться и повторяющиеся числа.
"""
int_var1: int = int(input())
int_var2: int = int(input())
int_var3: int = int(input())
if int_var1 >= int_var2:
    maximum = int_var1
else:
    maximum = int_var2
if int_var3 >= maximum:
    maximum = int_var3
print(maximum)
if int_var1 <= int_var2:
    minimum = int_var1
else:
    minimum = int_var2
if int_var3 <= minimum:
    minimum = int_var3
print(minimum)
print(int_var1 + int_var2 + int_var3 - minimum - maximum)
