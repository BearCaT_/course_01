"""
Напишите простой калькулятор, который считывает с пользовательского ввода
три строки: первое число, второе число и операцию, после чего применяет
операцию к введённым числам ("первое число" "операция" "второе число") и
выводит результат на экран.
Поддерживаемые операции: +, -, /, *, mod, pow, div, где
mod — это взятие остатка от деления,
pow — возведение в степень,
div — целочисленное деление.
Если выполняется деление и второе число равно 0, необходимо выводить
строку "Деление на 0!".Обратите внимание, что на вход программе приходят
вещественные числа.
"""
float_var1: float = float(input())
float_var2: float = float(input())
operation_name: str = input()
if operation_name == "+":
    print(float_var1 + float_var2)
elif operation_name == "-":
    print(float_var1 - float_var2)
elif operation_name == "*":
    print(float_var1 * float_var2)
elif operation_name == "pow":
    print(float_var1 ** float_var2)
if operation_name in ("/", "mod", "div"):
    if float_var2 == 0:
        print("Деление на 0!")
    elif operation_name == "/":
        print(float_var1 / float_var2)
    elif operation_name == "mod":
        print(float_var1 % float_var2)
    elif operation_name == "div":
        print(float_var1 // float_var2)
