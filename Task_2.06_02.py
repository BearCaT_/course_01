"""
Напишите программу, которая выводит часть последовательности 1 2 2 3 3 3 4 4 4 4 5 5 5 5 5 ...
(число повторяется столько раз, чему равно). На вход программе передаётся неотрицательное целое
число n — столько элементов последовательности должна отобразить программа. На выходе ожидается
последовательность чисел, записанных через пробел в одну строку.
Например, если n = 7, то программа должна вывести 1 2 2 3 3 3 4.
"""
# длинна последовательности
sequence_length: int = int(input())
# результат в виде строки
Result: str = ""
# текущее число последовательности
current_number: int = 0
# количество выведенных чисел
number_count: int = 0
# формирование строки результата
while number_count <= sequence_length:
    current_number += 1
    for i in range(current_number):
        number_count += 1
        Result += str(current_number) + " "
        if number_count == sequence_length:
            break
    if number_count == sequence_length:
        break
# вывод результата
print(Result)
