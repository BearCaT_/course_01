"""
Урок 1.8
Катя узнала, что ей для сна надо XX минут. В отличие от Коли,
Катя ложится спать после полуночи в HH часов и MM минут.
Помогите Кате определить, на какое время ей поставить будильник,
чтобы он прозвенел ровно через XX минут после того, как она ляжет
спать.
На стандартный ввод, каждое в своей строке, подаются значения XX,
HH и MM. Гарантируется, что Катя должна проснуться в тот же день,
что и заснуть. Программа должна выводить время, на которое нужно
поставить будильник: в первой строке часы, во второй — минуты.
"""
total_sleep_time_in_minutes = int(input())
start_sleep_time_in_hours = int(input())
start_sleep_time_in_minutes = int(input())
clock_time_h = start_sleep_time_in_hours + total_sleep_time_in_minutes // 60
clock_time_m = start_sleep_time_in_minutes + total_sleep_time_in_minutes % 60
clock_time_h += clock_time_m // 60
clock_time_m = clock_time_m % 60
print(clock_time_h)
print(clock_time_m)
