"""
Напишите программу, которая принимает на вход список чисел в одной строке и выводит
на экран в одну строку значения, которые встречаются в нём более одного раза.
Для решения задачи может пригодиться метод sort списка.
Выводимые числа не должны повторяться, порядок их вывода может быть произвольным.
"""
# списока чисел
number_list = [int(i) for i in input().split()]
number_list.sort()
# результат задачи - инициализация
result: str = ""
# конвертируем список чисел в строку
number_string: str = str(number_list)
# флаг - '0' первое число из списка
first_time_flag: int = 0
# цикл по списку чисел
for number in number_list:
    # если число в списке встречается больше одного раза и его нет в результате- выводим его
    if number_string.count(str(number)) > 1 and result.find(str(number)) == -1:
        if first_time_flag == 0:
            result += str(number)
            first_time_flag = 1
        else:
            result += " " + str(number)
# выводим результат
if result != "":
    print(result)
